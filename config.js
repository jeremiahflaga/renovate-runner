module.exports = {
    platform: 'gitlab',
    endpoint: 'https://gitlab.com/api/v4/',
    assignees: ['jeremiahflaga'],
    baseBranches: ['main'],
    labels: ['renovate'],
    extends: ['config:base']
};
